# fullstack-challenge

## How to run the app

1. Install the necessary packages and depencies: `npm i`.
2. Start up the application: `npm start`.

## How to run tests
You can run the tests: `npm test` in the CLI.

## Technologies

**Frontend**

ReactJS for building using

**Backend**

- ExpressJS

**Dependencies**

- Webpack and Babel

**Test Framework**

- Enzyme
- Mocha
- Chai