import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import App from '../app/components/App';
import Form from '../app/components/Form';
import InputField from '../app/components/InputField'
import Results from '../app/components/Results'
import SpinningLoader from '../app/components/SpinningLoader'

/**
 * Unit Tests for App component.
 */
describe('app.components.App', function () {
  beforeEach(function () {
    this.wrapper = shallow(<App />);
  });

  it('renders correctly', function () {
    expect(this.wrapper.find('.cp-app')).to.have.length(1);
  });

  it('renders `Form` component', function () {
    expect(this.wrapper.find(Form)).to.have.length(1);
  });
});

/**
 * Unit Tests for Form component.
 */
describe('app.components.Form', function () {
  beforeEach(function () {
    this.wrapper = shallow(<Form />);
  });

  it('renders correctly', function () {
    expect(this.wrapper.find('.cp-form')).to.have.length(1);
  });

  context('when `isLoading` state', function () {
    it('renders `SpinningLoader` component', function () {
      this.wrapper.setState({ isLoading: true })
      expect(this.wrapper.find(SpinningLoader)).to.have.length(1);
    });
  });

  context('when there are results', function () {
    it('renders `Results` component', function () {
      this.wrapper.setState({ results: [1,2,3] })
      expect(this.wrapper.find(Results)).to.have.length(1);
    });
  });

  context('when there are NO results', function () {
    it('does not render `Results` component', function () {
      this.wrapper.setState({ results: [] })
      expect(this.wrapper.find(Results)).to.have.length(0);
    });
  });
});

/**
 * Unit Tests for InputField component.
 */
describe('app.components.InputField', function () {
  beforeEach(function () {
    this.sandbox = sinon.createSandbox();
    this.props = {
      onChange: this.sandbox.spy()
    }
    this.wrapper = shallow(<InputField {...this.props} />);
  });

  it('renders correctly', function () {
    expect(this.wrapper.find('.cp-input-field')).to.have.length(1);
  });

  it('calls onChange when an input is entered', function () {
    this.wrapper
    .find('.form-control')
    .simulate('change', { target: { value: 2 } });
    expect(this.props.onChange.calledOnce).to.equal(true);
  });

  context('when there are errors', function () {
    beforeEach(function () {
      this.props = {
        onChange: this.sandbox.spy(),
        validity: 'test errors'
      };
      this.wrapper = shallow(<InputField {...this.props} />);
    });

    it('renders an error message', function () {
      expect(this.wrapper.find('.text-danger')).to.have.length(1);
    });
  });
});

/**
 * Unit Tests for Results component.
 */
describe('app.components.Results', function () {
  beforeEach(function () {
    this.props = {
      primes: [1,2]
    };
    this.wrapper = shallow(<Results {...this.props} />);
  });

  it('renders correctly', function () {
    expect(this.wrapper.find('.cp-results')).to.have.length(1);
  });
});

/**
 * Unit Tests for SpinningLoader component.
 */
describe('app.components.SpinningLoader', function () {
  beforeEach(function () {
    this.wrapper = shallow(<SpinningLoader />);
  });

  it('renders correctly', function () {
    expect(this.wrapper.find('.cp-spinning-loader')).to.have.length(1);
  });
});
