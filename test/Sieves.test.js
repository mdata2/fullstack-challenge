import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {
  expect
} from 'chai';
import isEqual from 'lodash/isEqual';

import {
  generatePrimes,
  median,
  isPrime
} from '../app/sieves';


describe('generatePrimes()', function () {
  it('returns an array', function () {
    expect(generatePrimes(10)).to.be.an('array');
  });

  it('returns an array of prime numbers', function () {
    expect(generatePrimes(10).every(n => isPrime(n))).to.be.true;
  });
});

describe('median()', function () {
  it('returns an array', function () {
    expect(generatePrimes(10)).to.be.an('array');
  });

  context('when length of the array is `odd`', function () {
    it('returns the middle value of the array', function () {
      expect(isEqual(median([2, 3, 5, 7]), [3, 5])).to.be.true;
    });
  });

  context('when length of the array is `even`', function () {
    it('returns the two middle most values of the array', function () {
      expect(isEqual(median([2, 3, 5, 7, 11, 13, 17], ), [7])).to.be.true;
    });
  });
});

Enzyme.configure({
  adapter: new Adapter()
});
