import express from 'express';
import http from 'http';
import path from 'path';
import bodyParser from 'body-parser';
import favicon from'serve-favicon';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from './webpack.config.js';

import {
  generatePrimes,
  median
} from './app/sieves';

// Setup express app
const app = express();

/**
 * This will display log to CLI.
 */
function log(message) {
  process.stdout.write(`${message}\n`);
}

// Setup webpack compiler to point to config
const compiler = webpack(webpackConfig);

// Express app config
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

// Use webpack-dev-middleware and use the webpack.config.js
app.use(webpackDevMiddleware(compiler, {
  noInfo: true,
  publicPath: webpackConfig.output.publicPath,
  stats: 'errors-only'
}));

app.use(webpackHotMiddleware(compiler));

// serve static files
app.use(express.static(path.join(__dirname + '/app/public')))
app.use(favicon(__dirname + '/app/public/images/favicon.ico'));

// Post Route for handling user input
app.post('/api/median', function (req, res) {
  const inputVal = req.body.inputVal;
  res.send(median(generatePrimes(inputVal)));
});

app.get('*', function(req, res, next) {
  setImmediate(() => { next(new Error('There is an error in the server')); });
});

/**
 * This is use to normalize a port into a number, string, or false.
 */
function normalizePort(val) {
  const port = parseInt(val, 10);
  if (Number.isNaN(port)) {
    // named pipe
    return val;
  }
  if (port >= 0) {
    // port number
    return port;
  }
  return false;
}

/**
 * Get port from environment and store in Express.
 */
const port = normalizePort(process.env.PORT || 3000);
app.set('port', port);

/**
 * HTTP server.
 */
const server = http.createServer(app);
let availablePort = port;

/**
 * This will listen on provided port for all network int.
 */
function startServer(serverPort) {
  server.listen(serverPort);
}

/**
 * Listens to HTTP server event.
 */
function onListening() {
  const addr = server.address();
  const bind = `${
    typeof addr === 'string' ? 'pipe' : 'port'
  } ${
    typeof addr === 'string' ? addr : addr.port
  }`;
  log(`Server is listening on ${bind}`);
  log(`Visit: http://localhost:${addr.port}`);
}

/**
 * Fireup server.
 */
server.on('listening', onListening);
startServer(availablePort);
