import React, { Component } from 'react';
import Form from '../components/Form';

import '../styles/App.scss';

export class App extends Component {
  render() {
    return (
      <div className="cp-app container">
        <h1>TouchBistro FullStack Challenge</h1>
        <p className="text-info">
          This program will find the median prime number(s) of the set of prime numbers less than n using Sieve of
					Eratosthenes algorithm.
				</p>
        <Form />
      </div>
    );
  }
}

export default App;
