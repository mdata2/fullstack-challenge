import React, { Component } from 'react'
import axios from 'axios';
import isEmpty from 'lodash/isEmpty';

import InputField from './InputField';
import Results from './Results';
import SpinningLoader from './SpinningLoader';

import '../styles/Form.scss';

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputVal: '',
      inputErr: '',
      results: [],
      isLoading: false
    }
    this.isFormValid = this.isFormValid.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.renderResults = this.renderResults.bind(this);
  }

  isFormValid() {
    const { inputVal, inputErr } = this.state;
    return inputVal && !inputErr;
  }

  handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({
      [name]: value,
      inputErr: null
    });
  }

  handleSubmit(e) {
    const { inputVal, inputErr } = this.state;
    e.preventDefault();

    if (this.isFormValid()) {
      this.setState({ isLoading: true });
      axios.post('/api/median', { inputVal })
        .then(res =>  this.setState({
          results: res.data,
          isLoading: false,
          inputErr: isEmpty(res.data) ? 'No results found.' : ''
        }))
        .catch(err => this.setState({
          inputErr: 'Server connection timeout. Please restart the server.',
          isLoading: false
        }))
    } else {
      const errorState = {};

      if (!inputVal) {
        errorState.inputErr = 'This is a required field.';
      }

      if (!isEmpty(errorState)) {
        this.setState(errorState);
      }
    }
  }

  renderResults() {
    const { results } = this.state;
    return (!isEmpty(results) && <Results primes={results} />);
  }

  render() {
    const { inputVal, inputErr, results, isLoading } = this.state;

    return ( 
      <div className="cp-form">
        <form onSubmit={this.handleSubmit}>
          <label htmlFor="number-input" className="input-field-num">
            Please enter a number: 
            <span className="required">*</span>
          </label>
          <InputField
            type="number"
            name="inputVal"
            value={inputVal}
            onChange={this.handleChange}
            validity={inputErr}
          />
          <button className="submit-btn btn btn-primary"> Find Median </button>
        </form>
        {isLoading && <SpinningLoader />}
        {this.renderResults()}
      </div>
    )
  }
}
