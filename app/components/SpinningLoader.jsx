import React from 'react';

export default function SpinningLoader() {
  return (
    <div className="cp-spinning-loader spinner-border" role="status">
      <span className="sr-only">Loading...</span>
    </div>
  )
}
