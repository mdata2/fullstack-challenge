import React from 'react'
import PropTypes from 'prop-types'

import '../styles/InputField.scss';

function renderErrMessage(message) {
  return (
    <div className="text-danger" role="alert">
      {message}
    </div>
  )
}

export default function InputField({ value, onChange, validity, ...props }) {
  const errorClass = validity ? 'field-error' : '';
  return (
    <div className="cp-input-field form-group">
      <input
        value={value}
        onChange={onChange}
        className={`form-control ${errorClass}`}
        {...props}
      />
      {renderErrMessage(validity)}
    </div>
  )
}

InputField.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
};
