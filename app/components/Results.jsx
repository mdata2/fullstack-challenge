import React from 'react';
import PropTypes from 'prop-types';

import '../styles/Results.scss';

export default function Results({ primes }) {
	return (
		<div className="cp-results">
			<span>The median values {primes.length > 1 ? 'are ' : 'is '}</span>
			{primes.map((n, index) => (
				<span className="result-item" key={`item-${index}`}>
					{n}
					{index < primes.length - 1 ? ', ' : ''}
				</span>
			))}
		</div>
	);
}

Results.propTypes = {
	primes: PropTypes.array.isRequired
};
