export function generatePrimes(nLimit) {
  /**
   * @param {Number} nLimit A number.
   * @return {Array} Generates an array of prime numbers less than nLimit.
   */
  let k;
  let l;

  var sieve = [];
  var primes = [];

  sieve[1] = false;

  for (k = 2; k <= nLimit; k += 1) {
    sieve[k] = true;
  }

  for (k = 2; k * k <= nLimit; k += 1) {
    if (sieve[k] !== true) {
      continue;
    }

    for (l = k * k; l <= nLimit; l += k) {
      sieve[l] = false;
    }
  }

  sieve.forEach(function (value, key) {
    if (value) {
      this.push(key);
    }
  }, primes);
  return primes;
};

export function median(nArray) {
  /**
   * @param {Array} nArray An array of numbers.
   * @return {Array} The calculated median value(s) from the specified array.
   */
  let median = [];
  const mid = Math.floor(nArray.length / 2);
  const numsLen = nArray.length;

  if (numsLen === 0) {
    return median;
  }

  if (numsLen % 2 === 0) {
    // two middle numbers
    median = [nArray[mid - 1], nArray[mid]];
  } else {
    // middle number 
    median = [nArray[mid]];
  }

  return median;
}

export function isPrime(value) {
  for (var i = 2; i < value; i++) {
    if (value % i === 0) {
      return false;
    }
  }
  return value > 1;
}

export default {
  generatePrimes,
  median,
  isPrime
}